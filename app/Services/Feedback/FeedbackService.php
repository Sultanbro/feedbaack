<?php

namespace App\Services\Feedback;

use App\DTO\FeedbackDTO;
use App\Models\Feedback;
use App\Services\Contracts\Feedback\FeedbackServiceContract;
use Illuminate\Support\Facades\Storage;

class FeedbackService implements FeedbackServiceContract
{
    public function storeFeedback(FeedbackDTO $feedbackDTO): bool
    {
        if ($feedbackDTO->file) {
            $filePath = $feedbackDTO->file->store('feedback_files');
            $feedbackDTO->file_path = $filePath;
        }

        Feedback::create([
            'name' => $feedbackDTO->name,
            'email' => $feedbackDTO->email,
            'phone' => $feedbackDTO->phone,
            'city' => $feedbackDTO->city,
            'title' => $feedbackDTO->title,
            'message' => $feedbackDTO->message,
            'file' => $feedbackDTO->file_path ?? null,
        ]);
        return true;
    }
}
