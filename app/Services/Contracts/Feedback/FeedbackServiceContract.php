<?php

namespace App\Services\Contracts\Feedback;

use App\DTO\FeedbackDTO;

interface FeedbackServiceContract
{
    public function storeFeedback(FeedbackDTO $feedbackDTO): bool;
}
