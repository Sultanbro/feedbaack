<?php

namespace App\DTO;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class FeedbackDTO
{
    public string $name;
    public string $email;
    public string $phone;
    public string $city;
    public string $title;
    public string $message;
    public ?UploadedFile $file;

    public function __construct(
        string $name,
        string $email,
        string $phone,
        string $city,
        string $title,
        string $message,
        ?UploadedFile $file = null
    ) {
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->city = $city;
        $this->title = $title;
        $this->message = $message;
        $this->file = $file;
    }

    public static function getRequest(Request $request): self
    {
        return new self(
            $request->input('name'),
            $request->input('email'),
            $request->input('phone'),
            $request->input('city'),
            $request->input('title'),
            $request->input('message'),
            $request->file('file')
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }
}
