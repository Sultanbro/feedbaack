<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CheckKeyToken
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (($this->token = $request->header('Authorization', null)) === null)
            return \response()->json(['token' => 'not found'], 401);
        $this->token = str_replace('Bearer ', '', $this->token);

        if($this->token === config('connect.token')) {
            return $next($request);
        }
        return \response()->json(['token' => 'not found'], 401);
    }
}
