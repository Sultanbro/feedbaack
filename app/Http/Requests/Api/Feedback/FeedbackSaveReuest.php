<?php

namespace App\Http\Requests\Api\Feedback;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackSaveReuest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'email:rfc,dns',
            'phone' => 'required|regex:/^\+77\d{9}$/',
            'city' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'message' => 'required|string',
            'file' => 'required|file|mimes:pdf,doc,jpg',
        ];
    }
}
