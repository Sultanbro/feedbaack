<?php

namespace App\Http\Controllers\Api;

use App\DTO\FeedbackDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Feedback\FeedbackSaveReuest;
use App\Services\Contracts\Feedback\FeedbackServiceContract;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    /**
     * @var FeedbackServiceContract
     */
    private $feedbackService;

    /**
     * FeedbackController constructor.
     * @param FeedbackServiceContract $feedbackService
     */
    public function __construct(FeedbackServiceContract $feedbackService)
    {
        $this->feedbackService = $feedbackService;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FeedbackSaveReuest $request)
    {
        try {
            return response()->json(['message' => 'Feedback stored successfully',
                'performances' => $this->feedbackService->storeFeedback(FeedbackDTO::getRequest($request))], 201);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

}
